import Vue from "vue";
import VueI18n from "vue-i18n";

Vue.use(VueI18n);

import en from "../locales/en_US.json";
import ko from "../locales/ko_KR.json";

const messages = {
  en,
  ko,
};

const i18n = new VueI18n({
  locale: "ko",
  messages,
});
export default i18n;
