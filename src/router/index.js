import Vue from "vue";
import VueRouter from "vue-router";
import MainComponents from "@/components/MainComponents";
import LoginComponents from "@/components/LoginComponents";
import AdminComponents from "@/components/AdminComponents";
import Main from "@/views/Main";
import About from "@/views/pages/About";
import Service from "@/views/pages/Service";
import Diabetes from "@/views/pages/Diabetes";
import Exercise from "@/views/pages/Exercise";
import Board from "@/views/pages/Board";
import BoardView from "@/views/pages/BoardView";

import AdminBoardList from "@/views/administrator/board/BoardList";
import AdminBoardGet from "@/views/administrator/board/BoardGet";
import AdminBoardAdd from "@/views/administrator/board/BoardAdd";
import AdminBoardRevise from "@/views/administrator/board/BoardRevise";

// import { requireAuth } from './requireAuth';

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "/",
    component: MainComponents,
    children: [
      {
        path: "/",
        name: "Main",
        component: Main,
      },
      {
        path: "about",
        name: "About",
        component: About,
      },
      {
        path: "service",
        name: "Service",
        component: Service,
      },
      {
        path: "diabetes",
        name: "Diabetes",
        component: Diabetes,
      },
      {
        path: "exercise",
        name: "Exercise",
        component: Exercise,
      },
      {
        path: "board/:boardType",
        name: "Board",
        component: Board,
      },
      {
        path: "board/view",
        name: "BoardView",
        component: BoardView,
      },
    ],
  },
  {
    path: "/admin/login",
    name: "Login",
    component: LoginComponents,
  },
  {
    path: "/admin",
    name: "Admin",
    component: AdminComponents,
    children: [
      {
        path: "boardList/:boardType",
        name: "AdminBoardList",
        component: AdminBoardList,
      },
      {
        path: "boardView",
        name: "AdminBoardView",
        component: AdminBoardGet,
      },
      {
        path: "boardRevise",
        name: "AdminBoardRevise",
        component: AdminBoardRevise,
      },
      {
        path: "boardAdd",
        name: "BoardAdd",
        component: AdminBoardAdd,
      },
    ],
  },
];
const router = new VueRouter({
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

export default router;
