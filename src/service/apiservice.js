import Vue from "vue";
// import qs from 'query-string';
// import GetBoardListRequest from '@/service/request/GetBoardListRequest';
// import GetBoardListResponse from "@/service/response/GetBoardListResponse";
// import AddBoardRequest from '@/service/request/AddBoardRequest';
// import AddBoardResponse from "@/service/response/AddBoardResponse";
// import ReviseBoardRequest from '@/service/request/ReviseBoardRequest';
// import ReviseBoardResponse from "@/service/response/ReviseBoardResponse";
// import RemoveBoardRequest from '@/service/request/RemoveBoardRequest';
// import RemoveBoardResponse from "@/service/response/RemoveBoardResponse";
// import GetBoardRequest from '@/service/request/GetBoardRequest';
// import GetBoardResponse from "@/service/response/GetBoardResponse";

export class ApiService {
  setAccessToken(accessToken /*: string*/) {
    Vue.axios.defaults.headers.common["Authorization"] = accessToken;
  }

  getBoardList(req /*: GetBoardListRequest*/) {
    // return (
    //   new Promise() <
    //   GetBoardListResponse >
    //   ((resolve, reject) => {
    //     Vue.axios.post <
    //       GetBoardListResponse >
    //       ("/api/v1/board/list", req).then((res) => {
    //         if (res.status === 200) {
    //           resolve(res.data);
    //           return;
    //         }
    //         console.error(res.status, res.statusText);
    //         reject(null);
    //       });
    //   })
    // );
    return new Promise((resolve, reject) => {
      Vue.axios.post("/api/v1/board/list", req).then((res) => {
        if (res.status === 200) {
          resolve(res.data);
          return;
        }
        console.error(res.status, res.statusText);
        reject(null);
      });
    });
  }

  addBoard(req /*: AddBoardRequest*/) {
    // return (
    //   new Promise() <
    //   AddBoardResponse >
    //   ((resolve, reject) => {
    //     Vue.axios.post <
    //       AddBoardResponse >
    //       ("/api/v1/board/add", req).then((res) => {
    //         if (res.status === 200) {
    //           resolve(res.data);
    //           return;
    //         }
    //         console.error(res.status, res.statusText);
    //         reject(null);
    //       });
    //   })
    // );S
    return new Promise((resolve, reject) => {
      Vue.axios.post("/api/v1/board/add", req).then((res) => {
        if (res.status === 200) {
          resolve(res.data);
          return;
        }
        console.error(res.status, res.statusText);
        reject(null);
      });
    });
  }

  reviseBoard(req /*: ReviseBoardRequest*/) {
    // return (
    //   new Promise() <
    //   ReviseBoardResponse >
    //   ((resolve, reject) => {
    //     Vue.axios.post <
    //       ReviseBoardResponse >
    //       ("/api/v1/board/revise", req).then((res) => {
    //         if (res.status === 200) {
    //           resolve(res.data);
    //           return;
    //         }
    //         console.error(res.status, res.statusText);
    //         reject(null);
    //       });
    //   })
    // );
    return new Promise((resolve, reject) => {
      Vue.axios.post("/api/v1/board/revise", req).then((res) => {
        if (res.status === 200) {
          resolve(res.data);
          return;
        }
        console.error(res.status, res.statusText);
        reject(null);
      });
    });
  }

  removeBoard(req /*: RemoveBoardRequest*/) {
    // return (
    //   new Promise() <
    //   RemoveBoardResponse >
    //   ((resolve, reject) => {
    //     Vue.axios.post <
    //       RemoveBoardResponse >
    //       ("/api/v1/board/remove", req).then((res) => {
    //         if (res.status === 200) {
    //           resolve(res.data);
    //           return;
    //         }
    //         console.error(res.status, res.statusText);
    //         reject(null);
    //       });
    //   })
    // );
    return new Promise((resolve, reject) => {
      Vue.axios.post("/api/v1/board/remove", req).then((res) => {
        if (res.status === 200) {
          resolve(res.data);
          return;
        }
        console.error(res.status, res.statusText);
        reject(null);
      });
    });
  }

  getBoard(req /*: GetBoardRequest*/) {
    // return (
    //   new Promise() <
    //   GetBoardResponse >
    //   ((resolve, reject) => {
    //     Vue.axios.post <
    //       GetBoardResponse >
    //       ("/api/v1/board/get", req).then((res) => {
    //         if (res.status === 200) {
    //           resolve(res.data);
    //           return;
    //         }
    //         console.error(res.status, res.statusText);
    //         reject(null);
    //       });
    //   })
    // );
    return new Promise((resolve, reject) => {
      Vue.axios.post("/api/v1/board/get", req).then((res) => {
        if (res.status === 200) {
          resolve(res.data);
          return;
        }
        console.error(res.status, res.statusText);
        reject(null);
      });
    });
  }
}
const _api = new ApiService();
ApiService.install = function (Vue /* options?: any */) {
  Vue.api = _api;
  Object.defineProperties(Vue.prototype, {
    $api: {
      get() {
        return _api;
      },
    },
    api: {
      get() {
        return _api;
      },
    },
  });
};

Vue.use(ApiService);

export default ApiService;
