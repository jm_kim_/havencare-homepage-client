

export default class Board {

  constructor() {

    this.boardCode = "";

    this.boardType = "";

    this.boardTitle = "";

    this.boardText = "";

    this.viewStartDate = "";

    this.viewEndDate = "";

    this.viewCount = 0;

    this.insertDt = "";

    this.updateDt = "";

    this.deleteDt = "";
  }
}
