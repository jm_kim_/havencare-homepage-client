

export default class GetBoardListRequest {

  constructor() {

    this.boardType = "";

    this.searchWord = "";

    this.viewStartDate = "";

    this.viewEndDate = "";
  }
}
