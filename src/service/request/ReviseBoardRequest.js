

export default class ReviseBoardRequest {

  constructor() {

    this.boardCode = "";

    this.boardType = "";

    this.boardTitle = "";

    this.boardText = "";

    this.viewStartDate = "";

    this.viewEndDate = "";
  }
}
