

export default class AddBoardRequest {

  constructor() {

    this.boardType = "";

    this.boardTitle = "";

    this.boardText = "";

    this.viewStartDate = "";

    this.viewEndDate = "";
  }
}
