import BaseResponse from '@/service/response/BaseResponse';


export default class RemoveBoardResponse extends BaseResponse {

  constructor() {
    super();
  }
}
