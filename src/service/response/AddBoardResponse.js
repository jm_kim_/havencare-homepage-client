import BaseResponse from '@/service/response/BaseResponse';


export default class AddBoardResponse extends BaseResponse {

  constructor() {
    super();

    this.board = null;
  }
}
