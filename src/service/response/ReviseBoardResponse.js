import BaseResponse from '@/service/response/BaseResponse';


export default class ReviseBoardResponse extends BaseResponse {

  constructor() {
    super();

    this.board = null;
  }
}
