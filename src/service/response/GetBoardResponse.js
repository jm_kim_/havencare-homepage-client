import BaseResponse from '@/service/response/BaseResponse';


export default class GetBoardResponse extends BaseResponse {

  constructor() {
    super();

    this.board = null;
  }
}
