import BaseResponse from '@/service/response/BaseResponse';


export default class GetBoardListResponse extends BaseResponse {

  constructor() {
    super();

    this.boards = null;
  }
}
