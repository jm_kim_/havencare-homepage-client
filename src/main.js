import Vue from "vue";

import "./plugins/axios";
import "./service/apiservice";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "./assets/css/reset.css";
import "./assets/css/common.css";
import "./assets/css/main.css";
import "./assets/css/responsive.css";
import "./assets/js/main.js";

import AOS from "aos";
import "aos/dist/aos.css";
import i18n from "./plugins/i18n";

Vue.config.productionTip = false;

new Vue({
  created() {
    AOS.init();
  },
  i18n,
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
