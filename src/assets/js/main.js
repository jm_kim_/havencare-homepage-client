window.onbeforeunload = function () {
  window.scrollTo(0, 0);
};

let last_known_scroll_position = 0;
let ticking = false;

function doSomething(scroll_pos) {
  // scroll 위치에 대한 작업을 하세요
  if (scroll_pos > 0) {
    document.querySelector(".header").classList.add("scrolled");
  } else {
    document.querySelector(".header").classList.remove("scrolled");
  }
}

window.addEventListener("scroll", function () {
  last_known_scroll_position = window.scrollY;

  if (!ticking) {
    window.requestAnimationFrame(function () {
      doSomething(last_known_scroll_position);
      ticking = false;
    });

    ticking = true;
  }
});
//
// //탭 클릭
// document.querySelectorAll(".tab > .tab-btn").forEach(function (item, index) {
//   item.addEventListener("click", function () {
//     //클릭하면 보이기
//     item
//       .closest(".tab")
//       .querySelectorAll(".tab-btn")
//       .forEach(function (item) {
//         item.classList.remove("on");
//       });
//     item.classList.add("on");
//
//     let tabIndex = index;
//
//     document.querySelectorAll(".tab-content").forEach(function (item) {
//       item
//         .closest("ul")
//         .querySelectorAll(".tab-content")
//         .forEach(function (item) {
//           item.classList.add("hide");
//         });
//       document
//         .querySelectorAll(".tab-content")
//         [tabIndex].classList.remove("hide");
//     });
//   });
// });
